package exercise.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DateUtils {
	
	private final static SimpleDateFormat MMM_D_YYYY_FORMAT = new SimpleDateFormat("MMM-d-yyyy");

	private final static TimeZone DEFAULT_TIMEZONE = TimeZone.getTimeZone("Asia/Manila");

	public static String formatDate(String inputDate) throws ParseException {
		if (null == inputDate || inputDate.equals(""))
			return "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date d = sdf.parse(inputDate);
		String formattedTime = MMM_D_YYYY_FORMAT.format(d);
		return formattedTime;
	}

	public static String formatDate(String inputDate, String format) throws ParseException {
		if (null == inputDate || inputDate.equals(""))
			return "";
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date d = sdf.parse(inputDate);
		String formattedTime = MMM_D_YYYY_FORMAT.format(d);
		return formattedTime;
	}

	public static long toEpochDate(String date, String format) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date d = sdf.parse(date);
		return d.getTime();
	}

	public static boolean theSameDay(long date1, long date2) {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
		return fmt.format(date1).equals(fmt.format(date2));
	}

	public static int getDaysBetween(String startDate, String endDate) throws ParseException {
		Calendar calStartDate = new GregorianCalendar();
		Calendar calEndDate = new GregorianCalendar();
		Date date = MMM_D_YYYY_FORMAT.parse(startDate);
		calStartDate.setTime(date);
		date = MMM_D_YYYY_FORMAT.parse(endDate);
		calEndDate.setTime(date);

		return (int) ((calEndDate.getTime().getTime() - calStartDate.getTime().getTime()) / (1000 * 60 * 60 * 24));
	}

	public static String getDateToday() {
		Calendar cal = new GregorianCalendar();
		cal.setTimeZone(DEFAULT_TIMEZONE);
		return MMM_D_YYYY_FORMAT.format(cal.getTime());
	}
}
