package exercise.core.components.content;

import java.text.ParseException;
import org.apache.sling.api.resource.ValueMap;
import com.adobe.cq.sightly.WCMUsePojo;
import exercise.core.DateUtils;

public class EventAdvertisement extends WCMUsePojo {

	private String eventTitle;

	private String image;

	private String startDate;

	private String endDate;

	private String shortDescription;

	private String[] locations;

	private String category;

	private String feeType;

	@Override
	public void activate() throws Exception {
		ValueMap props = getProperties();
		if (props != null) {
			this.eventTitle = props.get("eventTitle", "");
			this.image = props.get("eventImage", "");
			this.startDate = props.get("startDate", "");
			this.endDate = props.get("endDate", "");
			this.shortDescription = props.get("shortDescription", "");
			this.locations = props.get("locations", new String[] {});
			this.category = props.get("category", "");
			this.feeType = props.get("feeType", "");
		}
	}

	public String getEventTitle() {
		return eventTitle;
	}

	public void setEventTitle(String eventTitle) {
		this.eventTitle = eventTitle;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String[] getLocations() {
		return locations;
	}

	public void setLocations(String[] locations) {
		this.locations = locations;
	}

	public String getLocation() {
		String locationList = "";
		if (locations.length > 0)
			for (int i = 0; i < locations.length; i++)
				locationList += i == 0 ? locations[i] : ", " + locations[i];

		return locationList;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	public boolean isEmpty() {
		if (null == image || image.isEmpty())
			return true;
		return false;
	}

	public String getDisplayDate() {

		try {
			String startDate = DateUtils.formatDate(this.startDate);
			String endDate = DateUtils.formatDate(this.endDate);
			if (startDate.equalsIgnoreCase(endDate))
				return startDate;

			return startDate + " - " + endDate;
		} catch (ParseException e) {
			System.out.println("Error : getDisplayDate : " + e);
			return "";
		}

	}

	public String getFirstLocation() {
		if (locations.length > 0)
			return locations[0];
		return "";
	}
}
