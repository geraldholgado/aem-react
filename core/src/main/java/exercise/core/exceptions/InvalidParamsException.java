package exercise.core.exceptions;

public class InvalidParamsException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidParamsException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidParamsException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidParamsException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidParamsException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidParamsException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
