package exercise.core.models;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import exercise.core.DateUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {

	private final WeatherTemp temperature;

	private final ArrayList<WeatherInfo> weatherInfoList;

	private final String date;

	public Weather(@JsonProperty("dt_txt") String date, @JsonProperty("main") WeatherTemp temperature,
			@JsonProperty("weather") ArrayList<WeatherInfo> weatherInfoList) {
		super();
		this.date = date;
		this.temperature = temperature;
		this.weatherInfoList = weatherInfoList;
	}

	public String getDate() {
		return date;
	}

	public WeatherTemp getTemperature() {
		return temperature;
	}

	public ArrayList<WeatherInfo> getWeatherInfoList() {
		return weatherInfoList;
	}

	public String getDisplayDate() {
		try {
			return DateUtils.formatDate(date, "yyyy-MM-dd HH:mm:ss");
		} catch (ParseException e) {
			System.out.println("getDisplayDate : " + e);
			return "";
		}
	}

	public Boolean isDateToday() {
		try {
			if (DateUtils.theSameDay(new Date().getTime(), DateUtils.toEpochDate(date, "yyyy-MM-dd HH:mm:ss")))
				return true;
		} catch (ParseException e) {
			System.out.println("isDateToday : " + e);
			return false;
		}

		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((temperature == null) ? 0 : temperature.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Weather other = (Weather) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (temperature == null) {
			if (other.temperature != null)
				return false;
		} else if (!temperature.equals(other.temperature))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Weather [date=");
		builder.append(date);
		builder.append(", temperature=");
		builder.append(temperature);
		builder.append(", weatherInfoList=");
		builder.append(weatherInfoList);
		builder.append("]");
		return builder.toString();
	}

}
