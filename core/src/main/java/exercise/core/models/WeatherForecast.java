package exercise.core.models;

import java.text.ParseException;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import exercise.core.DateUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherForecast {

	private final String cod;
	
	private final String message;
	
	private final City city;

	private final ArrayList<Weather> weathers;

	private Weather weather;

	public WeatherForecast(@JsonProperty("cod") String cod, @JsonProperty("message") String message, @JsonProperty("city") City city, @JsonProperty("list") ArrayList<Weather> weathers) {
		super();
		this.city = city;
		this.weathers = weathers;
		this.cod = cod;
		this.message = message;
	}

	public City getCity() {
		return city;
	}
	
	public String getCod() {
		return cod;
	}

	public String getMessage() {
		return message;
	}

	@JsonIgnore
	public ArrayList<Weather> getWeathers() {
		return weathers;
	}

	public Weather getWeather() {
		return weather;
	}

	public void setWeather(String date) throws ParseException {
		for (Weather weather : this.weathers) {
			if (DateUtils.theSameDay(DateUtils.toEpochDate(weather.getDate(), "yyyy-MM-dd HH:mm:ss"),
					DateUtils.toEpochDate(date, "yyyy-MM-dd'T'HH:mm:ss"))) {

				this.weather = weather;
				break;
			}
		}
		this.weather = this.weather == null ? this.weathers.get(0) : this.weather;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((weather == null) ? 0 : weather.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeatherForecast other = (WeatherForecast) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (weather == null) {
			if (other.weather != null)
				return false;
		} else if (!weather.equals(other.weather))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WeatherForecast [city=");
		builder.append(city);
		builder.append(", weathers=");
		builder.append(weathers);
		builder.append("]");
		return builder.toString();
	}

}
