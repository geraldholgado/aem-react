package exercise.core.models;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherTemp {

	private final BigDecimal temp;

	public WeatherTemp(@JsonProperty("temp") String temp) {
		super();
		this.temp = new BigDecimal(temp);
	}

	public BigDecimal getTemp() {
		return temp;
	}

	public BigDecimal getCelcius() {
		return temp.subtract(new BigDecimal("273.15"));
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((temp == null) ? 0 : temp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeatherTemp other = (WeatherTemp) obj;
		if (temp == null) {
			if (other.temp != null)
				return false;
		} else if (!temp.equals(other.temp))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WeatherTemp [temp=");
		builder.append(temp);
		builder.append("]");
		return builder.toString();
	}
	
	
 
}
