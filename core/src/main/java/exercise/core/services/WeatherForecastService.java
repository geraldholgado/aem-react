package exercise.core.services;

import java.io.IOException;
import java.text.ParseException;
import com.fasterxml.jackson.core.JsonProcessingException;

import exercise.core.exceptions.InvalidAppKeyException;
import exercise.core.exceptions.InvalidParamsException;
import exercise.core.models.WeatherForecast;

public interface WeatherForecastService {

	public WeatherForecast getWeatherForecast(String city, String date)
			throws IOException, JsonProcessingException, ParseException, InvalidParamsException, InvalidAppKeyException;
}
