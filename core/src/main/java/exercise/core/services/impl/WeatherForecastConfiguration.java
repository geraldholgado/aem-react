package exercise.core.services.impl;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Weather Forecast Configuration", description = "Weather Forecast Configuration file")
public @interface WeatherForecastConfiguration {

	@AttributeDefinition(name = "API Key", description = "API key", type = AttributeType.PASSWORD)
	public String getAppkey() default "";

}
