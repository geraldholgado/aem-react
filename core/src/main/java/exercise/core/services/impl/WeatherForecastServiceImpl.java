package exercise.core.services.impl;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;

import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import exercise.core.exceptions.InvalidAppKeyException;
import exercise.core.exceptions.InvalidParamsException;
import exercise.core.models.WeatherForecast;
import exercise.core.services.WeatherForecastService;

@Component(service = WeatherForecastService.class, immediate = true, configurationPid = "exercise.core.services.impl.WeatherForecastServiceImpl", configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = WeatherForecastConfiguration.class)
public class WeatherForecastServiceImpl implements WeatherForecastService {

	private final static String WEATHER_FORECAST_URI = "http://api.openweathermap.org/data/2.5/forecast";

	private final static Logger logger = LoggerFactory.getLogger(WeatherForecastServiceImpl.class);

	private WeatherForecastConfiguration config;

	private String appKey;

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Activate
	protected final void activate(WeatherForecastConfiguration configuration) {
		logger.info("WEATHERFORECAST : calling activate method");
		this.config = configuration;
		appKey = config.getAppkey();
		logger.info("WEATHERFORECAST : activate method : appkey = " + appKey);
	}

	@Override
	public WeatherForecast getWeatherForecast(String city, String startDate)
			throws JsonProcessingException, ParseException, InvalidParamsException, InvalidAppKeyException {

		if ((null == city || city.equals("")) || (null == startDate || startDate.equals("")))
			throw new InvalidParamsException("Invalid parameter");

		logger.info("WEATHERFORECAST input : appKey = " + appKey + ", city = " + city + ", startDate = " + startDate);

		StringBuilder uri = new StringBuilder(WEATHER_FORECAST_URI);

		uri.append("?q=").append(city.trim());

		uri.append("&appid=").append(appKey.trim());

		logger.info(uri.toString());

		ObjectMapper objectMapper = new ObjectMapper();

		WeatherForecast weatherForecast;

		try {
			URL url = new URL(uri.toString());
			weatherForecast = objectMapper.readValue(url, WeatherForecast.class);
			weatherForecast.setWeather(startDate);
		} catch (IOException e) {
			throw new InvalidAppKeyException("Invalid appkey : " + appKey);
		}
		return weatherForecast;
	}
}
