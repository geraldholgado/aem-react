package exercise.core.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import exercise.core.exceptions.InvalidParamsException;
import exercise.core.services.WeatherForecastService;

@Component(service = Servlet.class, immediate = true, property = {
		Constants.SERVICE_DESCRIPTION + "=Weather Forecast Servlet",
		"sling.servlet.paths=/bin/servlets/stratpoint-demo/weather/index",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET, "sling.servlet.extensions=" + "html"

})
public class WeatherForecastServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;

	private final static Logger logger = LoggerFactory.getLogger(WeatherForecastServlet.class);
	@Reference
	private WeatherForecastService weatherForecastService;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		String city = request.getParameter("city");
		String date = request.getParameter("start-date");

		try {
			String resp = objectMapper.writeValueAsString(weatherForecastService.getWeatherForecast(city, date));
			logger.info("WEATHERFORECAST : resp : " + resp);
			response.getWriter().print(resp);
			response.getWriter().close();
		} catch (InvalidParamsException ipe) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, ipe.getMessage());
		} catch (Exception e) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}

}
