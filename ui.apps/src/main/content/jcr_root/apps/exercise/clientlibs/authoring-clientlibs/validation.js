(function (document, $, ns) {
    "use strict";
 
    $(document).on("click", ".cq-dialog-submit", function (e) {
        e.stopPropagation();
        e.preventDefault();
 
        var $form = $(this).closest("form.foundation-form"),
            startDate = $form.find("[name='./startDate']").val(),
            endDate = $form.find("[name='./endDate']").val();

        if( (new Date(startDate).getTime() > new Date(endDate).getTime())){
    		 ns.ui.helpers.prompt({
                title: Granite.I18n.get("Invalid Input"),
                message: "Invalid End Date",
                actions: [{
                    id: "Back",
                    text: "Back",
                    className: "coral-Button"
                }],
            callback: function (actionId) {
                if (actionId === "Back") {
                }
            }
        });

		}else{
                 $form.submit();
        }
    });
})(document, Granite.$, Granite.author);