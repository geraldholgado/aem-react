$(document).ready(function() {
	console.log("PAGE LOAD");

	getWeatherForecast();
});

/*function getWeatherForecast() {
	var city = $('#event-city').attr("value");
	console.log("event-city : " + city);
	var startDate = $('#event-start-date').attr("value");
	console.log("event-startDate : " + startDate);
	console.log("----getWeatherForecast---> " + city + " , " + startDate);
	$.ajax({
		url : "/bin/servlets/stratpoint-demo/weather/index",
		type : 'get',
		data : {
			"city" : city,
			"start-date" : startDate
		},
		success : function(data) {
			if(data.toUpperCase().startsWith("ERROR")) {
				console.log("ERROR");
				if(data.includes("503"))
					alert("Something went wrong. Please check the App Key");
				else
					alert("Something went wrong.");
			} else {
				console.log("SUCCESS");
				console.log(JSON.stringify(data));
				var json_resp = JSON.parse(data);
				var weather_json = json_resp.weather;
				console.log(JSON.stringify(weather_json));

				var temperature = weather_json.temperature.celcius;
				var weather_info_list = weather_json.weatherInfoList;
				var weather_info = weather_info_list[0];
				var weather = weather_info.main;
				var weather_desc = weather_info.description;
				var date = weather_json.displayDate;
				var isDateToday = weather_json.dateToday;

				var label = "Weather Today";
				if (isDateToday == false) {
					label = "Weather Forecast on " + date;
				}

				$("#weather-forecast-heading").html(label);
				$("#temperature").html(temperature + "&#8451;");
				$("#temp_desc").html(weather + "(" + weather_desc + ")");
			}
			

		}
	});
}*/


function getWeatherForecast() {


    $('.event-data').each(function(){
        var eventData = $(this);
        var city = eventData.find('#event-city').attr("value");
        console.log("event-city : " + city);
        var startDate = eventData.find('#event-start-date').attr("value");
        console.log("event-startDate : " + startDate);
        console.log("----getWeatherForecast---> " + city + " , " + startDate);

        $.ajax({
            url : "/bin/servlets/stratpoint-demo/weather/index",
            type : 'get',
            data : {
				"city" : city,
                "start-date" : startDate
            },
            success : function(data) {
                console.log("SUCCESS");
                console.log(JSON.stringify(data));
                var json_resp = JSON.parse(data);
                var weather_json = json_resp.weather;
                console.log(JSON.stringify(weather_json));
                
                var temperature = weather_json.temperature.celcius;
                var weather_info_list = weather_json.weatherInfoList;
                var weather_info = weather_info_list[0];
                var weather = weather_info.main;
                var weather_desc = weather_info.description;
                var date = weather_json.displayDate;
                var isDateToday = weather_json.dateToday;
                
                var label = "Weather Today";
                if (isDateToday == false) {
                    label = "Weather Forecast on " + date;
                }
                
                eventData.find('#weather-forecast-heading').html(label);
                eventData.find("#temperature").html(temperature + "&#8451;");
                eventData.find("#temp_desc").html(weather + "(" + weather_desc + ")");
            },
            error: function (jqXHR, exception) { 
                console.log("ERROR");
                
				if (jqXHR.status == 400) {
                    msg = 'Invalid Parameter. [400]';
                } else {
                    msg = 'Internal Server Error [500].';
                }

				eventData.html(msg);
            }
        });



    },1000);
}